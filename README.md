## Reflexiones

Reflexiones en torno al trabajo en el dictado de clases de Pensamiento Computacional.

Este repositorio está organizado a partir de este archivo README que funciona como índice de los documentos que creamos.

### Estructura del repo
- Cada artículo va en el raiz del repo. En el directorio *assets* van imágenes o cualquier tipo de archivo que necesitemos para los artículos que no sean los artículos mismos.
- La nomenclatura para los archivos es *año-mes-dia-titulo-del-archivo-separado-con-guiones.md*
- Para hacer comentarios a los artículos usamos los *issues* del repositorio.
- Para editar con otres *pull request*

## ïndice
- [Procedimientos en la enseñanza de Pensamiento Computacional](2020-02-03-procedimientos-en-la-ensenianza-de-pensamiento-computacional.md)
